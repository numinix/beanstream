BEANSTREAM PAYMENT MODULE FOR ZEN CART

-------------------------------------------------------------------------

Some portions Copyright (c) 2009 That Software Guy, Inc.  http://www.thatsoftwareguy.com.
Copyright (c) 2007-2009 Zen Ventures, LLC  http://www.zen-cart.com
Copyright (c) 2004 Beanstream <http://www.beanstream.com>,
Victoria, British Columbia, Canada

This module is written specifically to work with Zen Cart.
This software is licensed under GPL (see license file)

Version history below.
-------------------------------------------------------------------------

INSTALLATION:

To use this payment module to process transactions in your store, you will need
an account with Beanstream. You can reach them at www.beanstream.com.   If you 
wish to try it out before subscribing, contact their office and request a 
sandbox account.  Some basic contact information is all that is required.

When you sign up for your full live account, please TELL BEANSTREAM 
YOU WERE SENT TO THEM BY ZEN CART.  This helps support the Zen Cart project.

0. If you are running a version of the Beanstream Payment module prior 
  to version 1.04, go to Admin->Modules->Payment, select Beanstream,
  note your settings, and Remove the module.

1. Copy the enclosed files to your main zencart directory, retaining the existing folder structure.

2. Open your Zen Cart admin area and enable the Beanstream module (Admin->Modules->Payment->Beanstream).
   Click on Install to install the module.

3. Enter your Beanstream account number(s) provided by Beanstream.
   You will be given your Merchant Number in the email from Beanstream.

   This Beanstream module offers processing for Canadian Dollar (CAD) 
   or US Dollar (USD) transactions.  Beanstream requires separate
   account numbers for CAD and USD transactions; if you have a USD
   Merchant Number, enter it in the USD Merchant Number field; if you 
   have a CAD Merchant Number, enter it in the CAD Merchant Number field.

   Please ensure that you have a CAD and/or USD currency defined in your 
   Admin->Localization->Currencies as required.

   If you offer customer-selected CAD and USD sales using the 
   currency sidebox, and you have both USD and CAD account numbers, 
   the appropriate currency is selected.

   Otherwise, the store's default currency is used.

   The remaining settings are common for both accounts.

4. Leave the username and password fields BLANK for now.
   You can create transaction username/password settings at a later date
   and fill in these fields at that time.

   NOTE: The password supplied by Beanstream is NOT the same as the 
         username/password which can be entered in the Zen Cart module. 

5. Select whether you want to process transactions as Authorizations Only, 
   or Authorize-and-Capture.  Note that test transactions should use 
   Authorize-and-Capture.  

====================== Enable VBV/SC ====================================
6. Go to your beanstream account and enable VBV/SC.

7. Go to ZC Admin > Modules>Payment>Beanstream Canada and switch 'Enable VBV/SC' to On.

=========================================================================
-------------------------------------------------------------------------

CHANGES:
Version 1.05 - October 2009 - Report customer's IP address to Beanstream for risk scoring. 
Version 1.04 - April 2009 - Added functionality to allow Enable Database Storage (used to track success/fail of each attempted transaction)
Version 1.03 - March 2009 
   - Resolve bug introduced in 1.02
   - Consistently use "Merchant ID" to describe account number.
   - Permit use of USD and/or CAD.
Version 1.02 - August 2008 - Start counting products at 1.
Version 1.01 - January 2008 - Some bugs related to address-field-calculation resolved.
VERSION 1.0 - November 3, 2007 - Written for Zen Cart by the Zen Cart Team.
-------------------------------------------------------------------------

