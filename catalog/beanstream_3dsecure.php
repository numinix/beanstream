<?php
require('includes/application_top.php');

require(DIR_WS_MODULES . zen_get_module_directory('require_languages.php'));
require(DIR_WS_LANGUAGES . $_SESSION['language'] . '/' . $template_dir_select . 'checkout_process.php');

// Create an interac object for logging
require_once(DIR_WS_MODULES . "/payment/beanstream.php"); 
$lang_file = zen_get_file_directory(DIR_WS_LANGUAGES . $_SESSION['language'] . '/modules/payment/', 'beanstream.php', 'false');
if (@file_exists($lang_file)) {
   include_once($lang_file);
}

$submit_data = array('MD' => $_POST['MD'], 'PaRes' => $_POST['PaRes']);

$beanstream = new beanstream();



//Get Beanstream response codes into an array and close curl connection
$trnResult = $beanstream->_sendRequest($submit_data, true);

$response_code = $trnResult['messageId'];
$response_text = urldecode($trnResult['messageText']);
$beanstream->auth_code = $trnResult['authCode'];
$beanstream->transaction_id = $trnResult['trnId'] . ' Order Number Code: ' . $trnResult['trnOrderNumber'];
$response_msg_to_customer = '(' . $response_code . ') ' . $response_text . ($beanstream->commError == '' ? '' : ' Communications Error - Please notify webmaster.');

$order_time = date("F j, Y, g:i a"); // date('M-d-Y h:i:s')
$sessID = zen_session_id();

$beanstream->_debugActions($trnResult, $order_time, $sessID, true);
if ($trnResult['trnApproved'] === '0'  || $trnResult['messageText'] != 'Approved' || !preg_match('/trnApproved=1/', $beanstream->gwresponse) ) {
       $messageStack->add_session('checkout_payment', $response_msg_to_customer . ' - ' . MODULE_PAYMENT_BEANSTREAM_TEXT_DECLINED_MESSAGE, 'error');
       zen_redirect(zen_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL', true, false));
}

$beanstream->auth_code = $trnResult['authCode'];
$beanstream->transaction_id = $trnResult['trnId'];

$beanstream->_debugActions($trnResult, $order_time, $sessID);

// Setup from modules/checkout_process.php at start

// load selected payment module
require(DIR_WS_CLASSES . 'payment.php');
$payment_modules = new payment($_SESSION['payment']);

// load the selected shipping module
require(DIR_WS_CLASSES . 'shipping.php');
$shipping_modules = new shipping($_SESSION['shipping']);

require(DIR_WS_CLASSES . 'order.php');
$order = new order;

// prevent 0-entry orders from being generated/spoofed
if (sizeof($order->products) < 1) {
  zen_redirect(zen_href_link(FILENAME_SHOPPING_CART));
}

require(DIR_WS_CLASSES . 'order_total.php');
$order_total_modules = new order_total;
$order_totals = $order_total_modules->process();

// From: modules/checkout_process.php after before_process()

// create the order record
$insert_id = $order->create($order_totals, 2);

$zco_notifier->notify('NOTIFY_CHECKOUT_PROCESS_AFTER_ORDER_CREATE');
$payment_modules->after_order_create($insert_id);
$zco_notifier->notify('NOTIFY_CHECKOUT_PROCESS_AFTER_PAYMENT_MODULES_AFTER_ORDER_CREATE');
// store the product info to the order
$order->create_add_products($insert_id);
$_SESSION['order_number_created'] = $insert_id;
$zco_notifier->notify('NOTIFY_CHECKOUT_PROCESS_AFTER_ORDER_CREATE_ADD_PRODUCTS');
//send email notifications
$order->send_order_email($insert_id, 2);
$zco_notifier->notify('NOTIFY_CHECKOUT_PROCESS_AFTER_SEND_ORDER_EMAIL');


/**
 * Calculate order amount for display purposes on checkout-success page as well as adword campaigns etc
 * Takes the product subtotal and subtracts all credits from it
 */
  $credits_applied = 0;
  for ($i=0, $n=sizeof($order_totals); $i<$n; $i++) {
    if ($order_totals[$i]['code'] == 'ot_subtotal') $order_subtotal = $order_totals[$i]['value'];
    if ($$order_totals[$i]['code']->credit_class == true) $credits_applied += $order_totals[$i]['value'];
    if ($order_totals[$i]['code'] == 'ot_total') $ototal = $order_totals[$i]['value'];
  }
  $commissionable_order = ($order_subtotal - $credits_applied);
  $commissionable_order_formatted = $currencies->format($commissionable_order);
  $_SESSION['order_summary']['order_number'] = $insert_id;
  $_SESSION['order_summary']['order_subtotal'] = $order_subtotal;
  $_SESSION['order_summary']['credits_applied'] = $credits_applied;
  $_SESSION['order_summary']['order_total'] = $ototal;
  $_SESSION['order_summary']['commissionable_order'] = $commissionable_order;
  $_SESSION['order_summary']['commissionable_order_formatted'] = $commissionable_order_formatted;
  $_SESSION['order_summary']['coupon_code'] = $order->info['coupon_code'];
  $zco_notifier->notify('NOTIFY_CHECKOUT_PROCESS_HANDLE_AFFILIATES');

  // From: includes/modules/pages/checkout_process/header_php.php

// load the after_process function from the payment modules
  $_SESSION['transaction_id'] = $trnResult['trnId'];
  //$beanstream->after_process();
	
	// after_process code
	$sql = "insert into " . TABLE_ORDERS_STATUS_HISTORY . " (comments, orders_id, orders_status_id, date_added) values (:orderComments, :orderID, :orderStatus, now() )";
	$sql = $db->bindVars($sql, ':orderComments', 'Credit Card payment.  AUTH: ' . $trnResult['authCode'] . '. TransID: ' . $_SESSION['transaction_id'] . '.', 'string');
	$sql = $db->bindVars($sql, ':orderID', $insert_id, 'integer');
	$sql = $db->bindVars($sql, ':orderStatus', $beanstream->order_status, 'integer');
	$db->Execute($sql);

  $_SESSION['cart']->reset(true);

// unregister session variables used during checkout
  unset($_SESSION['sendto']);
  unset($_SESSION['billto']);
  unset($_SESSION['shipping']);
  unset($_SESSION['payment']);
  unset($_SESSION['comments']);
  $order_total_modules->clear_posts();//ICW ADDED FOR CREDIT CLASS SYSTEM

  // This should be before the zen_redirect:
  $zco_notifier->notify('NOTIFY_HEADER_END_CHECKOUT_PROCESS');

  zen_redirect(zen_href_link(FILENAME_CHECKOUT_SUCCESS, '', 'SSL'));

  require(DIR_WS_INCLUDES . 'application_bottom.php');
