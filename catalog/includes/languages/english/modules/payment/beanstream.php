<?php
/**
 * Beanstream API Payment Module Language definitions
 *
 * @package languageDefines
 * @copyright Copyright 2003-2009 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: beanstream_cad.php 54 2008-01-09 21:46:45Z drbyte $
 */


// Admin Configuration Items
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_ADMIN_TITLE', 'Beanstream Canada'); // Payment option title as displayed in the admin

  if (MODULE_PAYMENT_BEANSTREAM_STATUS == 'True') {
    define('MODULE_PAYMENT_BEANSTREAM_TEXT_DESCRIPTION', '<a target="_blank" href="https://www.beanstream.com/admin/sDefault.asp">Beanstream Merchant Login</a>' . (MODULE_PAYMENT_BEANSTREAM_TESTMODE != 'Production' ? '<br /><br />Testing Numbers:<br />Visa-Approved 4030000010001234<br />Visa-Approved $100 Limit 4504481742333<br />Visa-Declined 4003050500040005<br />MasterCard-Approved 5100000010001004<br />MasterCard-Approved 5194930004875020<br />MasterCard-Approved 5123450000002889<br />MasterCard-Declined 5100000020002000<br />Amex-Approved 371100001000131<br />Amex-Declined 342400001000180<br />Discover-Approved 6011500080009080<br />Discover-Declined 6011000900901111<br />Any future date can be used for the expiration date and any 3 (or 4 for AMEX) digit number can be used for the CVV Code.' : ''));
  } else {
    define('MODULE_PAYMENT_BEANSTREAM_TEXT_DESCRIPTION', '<a target="_blank" href="https://www.beanstream.com/website/merchant_services/application_form/apply.asp">Click Here to Sign Up for an Account</a><br /><br /><a target="_blank" href="https://www.beanstream.com/admin/sDefault.asp">Beanstream Merchant Login</a><br /><br /><strong>Requirements:</strong><br /><hr />*<strong>Beanstream Account</strong> (see link above to signup)<br />*<strong>CURL is required </strong>and MUST be compiled with SSL support into PHP by your hosting company<br />*<strong>Beanstream Merchant ID(s) and the Transaction username and password</strong> available from your Merchant Area under Order Settings.');
  }
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_ERROR_CURL_NOT_FOUND', 'CURL functions not found - required for Beanstream payment module');

// Catalog Items
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_CATALOG_TITLE', 'Credit Card');  // Payment option title as displayed to the customer
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_CREDIT_CARD_TYPE', 'Credit Card Type:');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_CREDIT_CARD_OWNER', 'Cardholder Name:');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_CREDIT_CARD_NUMBER', 'Credit Card Number:');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_CREDIT_CARD_EXPIRES', 'Expiry Date:');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_CVV', 'CVV Number:');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_POPUP_CVV_LINK', 'What\'s this?');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_JS_CC_OWNER', '* The owner\'s name from the credit card must be at least ' . CC_OWNER_MIN_LENGTH . ' characters.\n');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_JS_CC_NUMBER', '* The credit card number must be at least ' . CC_NUMBER_MIN_LENGTH . ' characters.\n');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_JS_CC_CVV', '* The 3 or 4 digit CVV number must be entered from the back of the credit card.\n');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_DECLINED_MESSAGE', 'Your credit card could not be authorized for this reason. Please correct the information and try again or contact us for further assistance.');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_ERROR', 'Credit Card Error!');

// admin tools:
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_REFUND_BUTTON_TEXT', 'Do Refund');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_REFUND_CONFIRM_ERROR', 'Error: You requested to do a refund but did not check the Confirmation box.');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_INVALID_REFUND_AMOUNT', 'Error: You requested a refund but entered an invalid amount.');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_CC_NUM_REQUIRED_ERROR', 'Error: You requested a refund but didn\'t enter the last 4 digits of the Credit Card number.');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_REFUND_INITIATED', 'Refund Initiated. Transaction ID: %s - Auth Code: %s');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_CAPTURE_CONFIRM_ERROR', 'Error: You requested to do a capture but did not check the Confirmation box.');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_CAPTURE_BUTTON_TEXT', 'Do Capture');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_INVALID_CAPTURE_AMOUNT', 'Error: You requested a capture but need to enter an amount.');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_TRANS_ID_REQUIRED_ERROR', 'Error: You need to specify a Transaction ID.');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_CAPT_INITIATED', 'Funds Capture initiated. Amount: %s.  Transaction ID: %s - Auth Code: %s');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_VOID_BUTTON_TEXT', 'Do Void');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_VOID_CONFIRM_ERROR', 'Error: You requested a Void but did not check the Confirmation box.');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_VOID_INITIATED', 'Void Initiated. Transaction ID: %s - Auth Code: %s ');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_AMOUNT_REQUIRED_ERROR', 'Please specify an amount.');

  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_REFUND_TITLE', '<strong>Refund Transactions</strong>');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_REFUND', 'You may refund money to the customer\'s credit card here:');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_REFUND_CONFIRM_CHECK', 'Check this box to confirm your intent: ');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_REFUND_AMOUNT_TEXT', 'Enter the amount you wish to refund');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_REFUND_CC_NUM_TEXT', 'Enter the last 4 digits of the Credit Card you are refunding.');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_REFUND_TRANS_ID', 'Enter the original Transaction ID:');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_REFUND_TEXT_COMMENTS', 'Notes (will show on Order History):');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_REFUND_DEFAULT_MESSAGE', 'Refund Issued');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_REFUND_SUFFIX', 'You may refund an order up to the amount already captured. You must supply the last 4 digits of the credit card number used on the initial order.');

  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_CAPTURE_TITLE', '<strong>Capture Transactions</strong>');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_CAPTURE', 'You may capture previously-authorized funds here:');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_CAPTURE_AMOUNT_TEXT', 'Enter the amount to Capture: ');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_CAPTURE_CONFIRM_CHECK', 'Check this box to confirm your intent: ');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_CAPTURE_TRANS_ID', 'Enter the original Transaction ID: ');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_CAPTURE_TEXT_COMMENTS', 'Notes (will show on Order History):');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_CAPTURE_DEFAULT_MESSAGE', 'Settled previously-authorized funds.');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_CAPTURE_SUFFIX', 'Captures should be performed within 2-10 days of the original authorization. You may only capture an order ONCE. <br />Please be sure the amount specified is correct.<br />If you leave the amount blank, the original amount will be used instead.');

  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_VOID_TITLE', '<strong>Voiding Transactions</strong>');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_VOID', 'You may void a transaction which has not yet been settled:');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_VOID_AMOUNT_TEXT', 'Enter the amount of the transaction being voided: ');
  define('MODULE_PAYMENT_BEANSTREAM_TEXT_VOID_CONFIRM_CHECK', 'Check this box to confirm your intent:');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_VOID_TEXT_COMMENTS', 'Notes (will show on Order History):');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_VOID_DEFAULT_MESSAGE', 'Transaction Canceled');
  define('MODULE_PAYMENT_BEANSTREAM_ENTRY_VOID_SUFFIX', 'Voids must be completed before the original transaction is settled in the daily batch, which occurs daily at 12:01am EST.');
