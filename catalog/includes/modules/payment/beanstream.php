<?php
/**
 * beanstream API payment method class
 *
 * @package paymentMethod
 * @copyright Copyright 2003-2009 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: beanstream_cad.php 55 2008-01-28 17:35:49Z drbyte $
 */
if (!defined('TABLE_BEANSTREAM')) define('TABLE_BEANSTREAM', DB_PREFIX . 'beanstream');
/**
 * Beanstream Payment Module (API version)
 * You must have SSL active on your server to be compliant with merchant TOS
 * CURL must be compiled into PHP with OpenSSL support in order for this module to work
 */
class beanstream extends base {
  /**
   * $code determines the internal 'code' name used to designate "this" payment module
   *
   * @var string
   */
  var $code;
  /**
   * $title is the displayed name for this payment method
   *
   * @var string
   */
  var $title;
  /**
   * $description is a soft name for this payment method
   *
   * @var string
   */
  var $description;
  /**
   * $enabled determines whether this module shows or not... in catalog.
   *
   * @var boolean
   */
  var $enabled;
  /**
   * log file folder
   *
   * @var string
   */
  var $_logDir = '';
  /**
   * communication vars
   */
  var $gwresponse = '';
  var $commErrNo = 0;
  var $commError = '';
  /**
   * debug content var
   */
  var $reportable_submit_data = array();
  /**
   * Constructor
   *
   * @return beanstream
   */
  function beanstream() {
    global $order, $messageStack;
    $this->enabled = ((MODULE_PAYMENT_BEANSTREAM_STATUS == 'True') ? true : false); // Whether the module is installed or not
    $this->currency_code = 'CAD';
    $this->code = 'beanstream';
    $this->login = MODULE_PAYMENT_BEANSTREAM_CAD_LOGIN;

    if (IS_ADMIN_FLAG === true) {
      // Payment module title in Admin
      $this->title = MODULE_PAYMENT_BEANSTREAM_TEXT_ADMIN_TITLE;
      if (MODULE_PAYMENT_BEANSTREAM_STATUS == 'True' && ($this->login == 'testing' || MODULE_PAYMENT_BEANSTREAM_TXNKEY == 'Test')) {
        $this->title .=  '<span class="alert"> (Not Configured)</span>';
      }
      if ($this->enabled && !function_exists('curl_init')) $messageStack->add_session(MODULE_PAYMENT_BEANSTREAM_TEXT_ERROR_CURL_NOT_FOUND, 'error');
    } else {
      $this->title = MODULE_PAYMENT_BEANSTREAM_TEXT_CATALOG_TITLE; // Payment module title in Catalog
    }
    $this->description = MODULE_PAYMENT_BEANSTREAM_TEXT_DESCRIPTION; // Descriptive Info about module in Admin
    $this->sort_order = MODULE_PAYMENT_BEANSTREAM_SORT_ORDER; // Sort Order of this payment option on the customer payment page
    $this->form_action_url = zen_href_link(FILENAME_CHECKOUT_PROCESS, '', 'SSL', false); // Page to go to upon submitting page info
    $this->order_status = (int)DEFAULT_ORDERS_STATUS_ID;
    if ((int)MODULE_PAYMENT_BEANSTREAM_ORDER_STATUS_ID > 0) {
      $this->order_status = (int)MODULE_PAYMENT_BEANSTREAM_ORDER_STATUS_ID;
    }

    $this->_logDir = DIR_FS_SQL_CACHE;

    if (is_object($order)) $this->update_status();
  }
  /**
   * calculate zone matches and flag settings to determine whether this module should display to customers or not
   *
   */
  function update_status() {
    global $order, $db;
    if ( ($this->enabled == true) && ((int)MODULE_PAYMENT_BEANSTREAM_ZONE > 0) ) {
      $check_flag = false;
      $check = $db->Execute("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_PAYMENT_BEANSTREAM_ZONE . "' and zone_country_id = '" . $order->billing['country']['id'] . "' order by zone_id");
      while (!$check->EOF) {
        if ($check->fields['zone_id'] < 1) {
          $check_flag = true;
          break;
        } elseif ($check->fields['zone_id'] == $order->billing['zone_id']) {
          $check_flag = true;
          break;
        }
        $check->MoveNext();
      }

      if ($check_flag == false) {
        $this->enabled = false;
      }
    }
  }
  /**
   * JS validation which does error-checking of data-entry if this module is selected for use
   * (Number, Owner, and CVV Lengths)
   *
   * @return string
   */
  function javascript_validation() {
    $js = '  if (payment_value == "' . $this->code . '") {' . "\n" .
    '    var cc_owner = document.checkout_payment.beanstream_cc_owner.value;' . "\n" .
    '    var cc_number = document.checkout_payment.beanstream_cc_number.value;' . "\n";
    if (MODULE_PAYMENT_BEANSTREAM_USE_CVV == 'True')  {
      $js .= '    var cc_cvv = document.checkout_payment.beanstream_cc_cvv.value;' . "\n";
    }
    $js .= '    if (cc_owner == "" || cc_owner.length < ' . CC_OWNER_MIN_LENGTH . ') {' . "\n" .
    '      error_message = error_message + "' . MODULE_PAYMENT_BEANSTREAM_TEXT_JS_CC_OWNER . '";' . "\n" .
    '      error = 1;' . "\n" .
    '    }' . "\n" .
    '    if (cc_number == "" || cc_number.length < ' . CC_NUMBER_MIN_LENGTH . ') {' . "\n" .
    '      error_message = error_message + "' . MODULE_PAYMENT_BEANSTREAM_TEXT_JS_CC_NUMBER . '";' . "\n" .
    '      error = 1;' . "\n" .
    '    }' . "\n";
    if (MODULE_PAYMENT_BEANSTREAM_USE_CVV == 'True')  {
      $js .= '    if (cc_cvv == "" || cc_cvv.length < "3" || cc_cvv.length > "4") {' . "\n".
      '      error_message = error_message + "' . MODULE_PAYMENT_BEANSTREAM_TEXT_JS_CC_CVV . '";' . "\n" .
      '      error = 1;' . "\n" .
      '    }' . "\n" ;
    }
    $js .= '  }' . "\n";

    return $js;
  }
  /**
   * Display Credit Card Information Submission Fields on the Checkout Payment Page
   *
   * @return array
   */
  function selection() {
    global $order;

    for ($i=1; $i<13; $i++) {
      $expires_month[] = array('id' => sprintf('%02d', $i), 'text' => strftime('%B - (%m)',mktime(0,0,0,$i,1,2000)));
    }

    $today = getdate();
    for ($i=$today['year']; $i < $today['year']+10; $i++) {
      $expires_year[] = array('id' => strftime('%y',mktime(0,0,0,1,1,$i)), 'text' => strftime('%Y',mktime(0,0,0,1,1,$i)));
    }
    $onFocus = ' onfocus="methodSelect(\'pmt-' . $this->code . '\')"';

    $selection = array('id' => $this->code,
                       'module' => MODULE_PAYMENT_BEANSTREAM_TEXT_CATALOG_TITLE,
                       'fields' => array(array('title' => MODULE_PAYMENT_BEANSTREAM_TEXT_CREDIT_CARD_OWNER,
                                               'field' => zen_draw_input_field('beanstream_cc_owner', $order->billing['firstname'] . ' ' . $order->billing['lastname'], 'id="'.$this->code.'-cc-owner"'. $onFocus),
                                               'tag' => $this->code.'-cc-owner'),
                                         array('title' => MODULE_PAYMENT_BEANSTREAM_TEXT_CREDIT_CARD_NUMBER,
                                               'field' => zen_draw_input_field('beanstream_cc_number', '', 'id="'.$this->code.'-cc-number"' . $onFocus),
                                               'tag' => $this->code.'-cc-number'),
                                         array('title' => MODULE_PAYMENT_BEANSTREAM_TEXT_CREDIT_CARD_EXPIRES,
                                               'field' => zen_draw_pull_down_menu('beanstream_cc_expires_month', $expires_month, '', 'id="'.$this->code.'-cc-expires-month"' . $onFocus) . '&nbsp;' . zen_draw_pull_down_menu('beanstream_cc_expires_year', $expires_year, '', 'id="'.$this->code.'-cc-expires-year"' . $onFocus),
                                               'tag' => $this->code.'-cc-expires-month')));
    if (MODULE_PAYMENT_BEANSTREAM_USE_CVV == 'True') {
      $selection['fields'][] = array('title' => MODULE_PAYMENT_BEANSTREAM_TEXT_CVV,
                                   'field' => zen_draw_input_field('beanstream_cc_cvv', '', 'size="4", maxlength="4"' . ' id="'.$this->code.'-cc-cvv"' . $onFocus) . ' ' . '<a href="javascript:popupWindow(\'' . zen_href_link(FILENAME_POPUP_CVV_HELP) . '\')">' . MODULE_PAYMENT_BEANSTREAM_TEXT_POPUP_CVV_LINK . '</a>',
                                   'tag' => $this->code.'-cc-cvv');
    }
    return $selection;
  }
  /**
   * Evaluates the Credit Card Type for acceptance and the validity of the Credit Card Number & Expiration Date
   *
   */
  function pre_confirmation_check() {
    global $messageStack;

    include(DIR_WS_CLASSES . 'cc_validation.php');

    $cc_validation = new cc_validation();
    $result = $cc_validation->validate($_POST['beanstream_cc_number'], $_POST['beanstream_cc_expires_month'], $_POST['beanstream_cc_expires_year'], $_POST['beanstream_cc_cvv']);
    $error = '';
    switch ($result) {
      case -1:
      $error = sprintf(TEXT_CCVAL_ERROR_UNKNOWN_CARD, substr($cc_validation->cc_number, 0, 4));
      break;
      case -2:
      case -3:
      case -4:
      $error = TEXT_CCVAL_ERROR_INVALID_DATE;
      break;
      case false:
      $error = TEXT_CCVAL_ERROR_INVALID_NUMBER;
      break;
    }

    if ( ($result == false) || ($result < 1) ) {
      $payment_error_return = 'payment_error=' . $this->code . '&beanstream_cc_owner=' . urlencode($_POST['beanstream_cc_owner']) . '&beanstream_cc_expires_month=' . $_POST['beanstream_cc_expires_month'] . '&beanstream_cc_expires_year=' . $_POST['beanstream_cc_expires_year'];
      $messageStack->add_session('checkout_payment', $error . '<!-- ['.$this->code.'] -->', 'error');
      zen_redirect(zen_href_link(FILENAME_CHECKOUT_PAYMENT, $payment_error_return, 'SSL', true, false));
    }

    $this->cc_card_type = $cc_validation->cc_type;
    $this->cc_card_number = $cc_validation->cc_number;
    $this->cc_expiry_month = $cc_validation->cc_expiry_month;
    $this->cc_expiry_year = $cc_validation->cc_expiry_year;
  }
  /**
   * Display Credit Card Information on the Checkout Confirmation Page
   *
   * @return array
   */
  function confirmation() {
    $confirmation = array('fields' => array(array('title' => MODULE_PAYMENT_BEANSTREAM_TEXT_CREDIT_CARD_TYPE,
                                                  'field' => $this->cc_card_type),
                                            array('title' => MODULE_PAYMENT_BEANSTREAM_TEXT_CREDIT_CARD_OWNER,
                                                  'field' => $_POST['beanstream_cc_owner']),
                                            array('title' => MODULE_PAYMENT_BEANSTREAM_TEXT_CREDIT_CARD_NUMBER,
                                                  'field' => substr($this->cc_card_number, 0, 4) . str_repeat('X', (strlen($this->cc_card_number) - 8)) . substr($this->cc_card_number, -4)),
                                            array('title' => MODULE_PAYMENT_BEANSTREAM_TEXT_CREDIT_CARD_EXPIRES,
                                                  'field' => strftime('%B, %Y', mktime(0,0,0,$_POST['beanstream_cc_expires_month'], 1, '20' . $_POST['beanstream_cc_expires_year']))) ));
    return $confirmation;
  }
  /**
   * Build the data and actions to process when the "Submit" button is pressed on the order-confirmation screen.
   * This sends the data to the payment gateway for processing.
   * (These are hidden fields on the checkout confirmation page)
   *
   * @return string
   */
  function process_button() {
    $process_button_string = zen_draw_hidden_field('cc_owner', $_POST['beanstream_cc_owner']) .
                             zen_draw_hidden_field('cc_expires', $this->cc_expiry_month . substr($this->cc_expiry_year, -2)) .
                             zen_draw_hidden_field('cc_type', $this->cc_card_type) .
                             zen_draw_hidden_field('cc_number', $this->cc_card_number);
    if (MODULE_PAYMENT_BEANSTREAM_USE_CVV == 'True') {
      $process_button_string .= zen_draw_hidden_field('cc_cvv', $_POST['beanstream_cc_cvv']);
    }
    $process_button_string .= zen_draw_hidden_field(zen_session_name(), zen_session_id());

    return $process_button_string;
  }
  /**
   * Store the CC info to the order and process any results that come back from the payment gateway
   *
   */
  function before_process() {
    global $response, $db, $order, $currencies, $messageStack;

    // Determine currency and account number to use.
    if ($order->info['currency'] == 'USD') {
           $this->currency_code = 'USD';
           $this->login = MODULE_PAYMENT_BEANSTREAM_USD_LOGIN;
    }

    $order->info['cc_number']  = str_pad(substr($_POST['cc_number'], -4), strlen($_POST['cc_number']), "X", STR_PAD_LEFT);
    $order->info['cc_expires'] = $_POST['cc_expires'];
    $order->info['cc_type']    = $_POST['cc_type'];
    $order->info['cc_owner']   = $_POST['cc_owner'];
    $order->info['cc_cvv']     = ''; //$_POST['cc_cvv'];
    $sessID = zen_session_id();

    // DATA PREPARATION SECTION
    // Set the order time
    $order_time = date("F j, Y, g:i a"); // date('M-d-Y h:i:s')

    // Calculate the next expected order id
    $last_order_id = $db->Execute("select * from " . TABLE_ORDERS . " order by orders_id desc limit 1");
    $new_order_id = $last_order_id->fields['orders_id'];
    $new_order_id = ($new_order_id + 1);
    // add randomized suffix to order id to produce uniqueness ... since it's unwise to submit the same order-number twice to the gateway
    $new_order_id = (string)$new_order_id . '-' . zen_create_random_value(6);

    unset($submit_data);  // Cleans out any previous data stored in the variable

    $province_code_order = (in_array($order->billing['country']['iso_code_2'], array('CA', 'US')) ? zen_get_zone_code($order->billing['country']['id'], $order->billing['state'], '--') : '--');
    if (strlen($order->billing['state']) > 2) {
      $sql = "SELECT zone_code FROM " . TABLE_ZONES . " WHERE zone_name = :zoneName";
      $sql = $db->bindVars($sql, ':zoneName', $order->billing['state'], 'string');
      $state = $db->Execute($sql);
      $province_code_order = (!$state->EOF) ? $state->fields['zone_code'] : '--';
    }
    $province_code_ship = (in_array($order->delivery['country']['iso_code_2'], array('CA', 'US')) ? zen_get_zone_code($order->delivery['country']['id'], $order->delivery['state'], '--') : '--');
    if (strlen($order->delivery['state']) > 2) {
      $sql = "SELECT zone_code FROM " . TABLE_ZONES . " WHERE zone_name = :zoneName";
      $sql = $db->bindVars($sql, ':zoneName', $order->delivery['state'], 'string');
      $state = $db->Execute($sql);
      $province_code_ship = (!$state->EOF) ? $state->fields['zone_code'] : '--';
    }

    // Populate an array that contains all of the data to be sent to the gateway
    $trans_data = array(
                         'trnType' => MODULE_PAYMENT_BEANSTREAM_AUTHORIZATION_TYPE == 'Authorize' ? 'PA': 'P',
                         'paymentMethod' => 'CC',  // (or IO for Interac)
                         'trnOrderNumber' => substr($new_order_id, 0, 30),
                         'trnCardOwner' => $order->info['cc_owner'],
                         'trnCardNumber' => $_POST['cc_number'],
                         'trnExpMonth' => substr($order->info['cc_expires'], 0, 2),
                         'trnExpYear' => substr($order->info['cc_expires'], -2),
                         'trnCardCvd' => $_POST['cc_cvv'],
                         'trnAmount' => number_format($currencies->value($order->info['total'], true, $this->currency_code), 2, '.', ''),
                         'ordItemPrice' => number_format($currencies->value($order->info['subtotal'], true, $this->currency_code), 2, '.', ''),
                         'ordShippingPrice' => number_format($currencies->value($order->info['shipping_cost'], true, $this->currency_code), 2, '.', ''),
                         'ordTax1Price' => number_format($currencies->value($order->info['tax'], true, $this->currency_code), 2, '.', ''),
                         'ordName' => $order->customer['firstname'] . ' ' . $order->customer['lastname'],
                         'ordAddress1' => $order->billing['street_address'],
                         'ordAddress2' => $order->billing['suburb'],
                         'ordCity' => $order->billing['city'],
                         'ordProvince' => $province_code_order,
                         'ordPostalCode' => $order->billing['postcode'],
                         'ordCountry' => $order->billing['country']['iso_code_2'],
                         'ordPhoneNumber' => $order->customer['telephone'],
                         'ordEmailAddress' => $order->customer['email_address']);
    if (!isset($_POST['cc_cvv'])) unset($trans_data['trnCardCvd']);
    if (isset($order->delivery['street_address']) && $order->delivery['street_address'] != '') {
      $shipping_data = array(
//                         'deliveryEstimate' => $order->delivery['shipping_method'],
                         'shipName' => $order->delivery['firstname'] . ' ' . $order->delivery['lastname'],
                         'shipAddress1' => $order->delivery['street_address'],
                         'shipAddress2' => $order->delivery['suburb'],
                         'shipCity' => $order->delivery['city'],
                         'shipProvince' => $province_code_ship,
                         'shipPostalCode' => $order->delivery['postcode'],
                         'shipCountry' => $order->delivery['country']['iso_code_2'],
                         'shippingMethod' => $order->info['shipping_method']);
    } else {
      $shipping_data = array();
    }
    $extra_data = array(
                         'shippingMethod' => $order->info['shipping_method'],
                         'trnComments' => 'Website Order', // $order->info['comments'],
                         // Additional Merchant-defined variables go here
                         'ref1' => $_SESSION['customer_id'],
                         'ref2' => $order_time,
                         'ref3' => zen_get_ip_address(),
                         'ref4' => $sessID,
                         'customerIp' => zen_get_ip_address(),
												 //'termUrl' => 'http://www.12fret.com/beanstream_3dsecure.php'
                          );
		if( strstr(MODULE_PAYMENT_BEANSTREAM_ENABLE_VBVSC,'On') )
		{
			$extra_data['termUrl'] = 'https://www.12fret.com/beanstream_3dsecure.php';
		}
    // itemized contents
    $prods_data = array();
    for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
      $j = $i + 1;
      $prods_data = array_merge($prods_data, array(
                         'prod_id_' .$j => substr($order->products[$i]['id'], 0, 32),
                         'prod_name_' .$j => substr(htmlentities($order->products[$i]['name'], ENT_QUOTES, 'UTF-8'), 0, 64),
                         'prod_quantity_' .$j  => $order->products[$i]['qty'],
                         'prod_cost_' .$j =>  number_format($currencies->value($order->products[$i]['price'], true, $this->currency_code), 2, '.', '') ));
    }

    $submit_data = array_merge($trans_data, $shipping_data, $extra_data, $prods_data);
    unset($response);
    $response = $this->_sendRequest($submit_data);
		// check if responseType is R(Redirect). That means that the card is VBV or SC enabled.
		// The message body of the response will contain Javascript to redirect the client's browser
		// to VBV or SC bank portal. The bank will send the response back to 
		// catalog/beanstream/beanstream.php. 
		if( $response['responseType'] == 'R' )
		{
			echo urldecode($response['pageContents']);
			exit();
		}		
    $response_code = $response['messageId'];
    $response_text = urldecode($response['messageText']);
    $this->auth_code = $response['authCode'];
    $this->transaction_id = $response['trnId'] . ' Order Number Code: ' . $response['trnOrderNumber'];
    $response_msg_to_customer = '(' . $response_code . ') ' . $response_text . ($this->commError == '' ? '' : ' Communications Error - Please notify webmaster.');

    $this->_debugActions($response, $order_time, $sessID);

    // If the response code is not 1 (approved) then redirect back to the payment page with the appropriate error message
    if ($response['trnApproved'] == 0 || $response['errorType'] != 'N') {
      $messageStack->add_session('checkout_payment', $response_msg_to_customer . ' - ' . MODULE_PAYMENT_BEANSTREAM_TEXT_DECLINED_MESSAGE, 'error');
      zen_redirect(zen_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL', true, false));
    }
  }
  /**
   * Post-process activities. Updates the order-status history data with the auth code from the transaction.
   *
   * @return boolean
   */
  function after_process() {
    global $insert_id, $db;
    $sql = "insert into " . TABLE_ORDERS_STATUS_HISTORY . " (comments, orders_id, orders_status_id, date_added) values (:orderComments, :orderID, :orderStatus, now() )";
    $sql = $db->bindVars($sql, ':orderComments', 'Credit Card payment.  AUTH: ' . $this->auth_code . '. TransID: ' . $this->transaction_id . '.', 'string');
    $sql = $db->bindVars($sql, ':orderID', $insert_id, 'integer');
    $sql = $db->bindVars($sql, ':orderStatus', $this->order_status, 'integer');
    $db->Execute($sql);
    return false;
  }
  /**
    * Build admin-page components
    *
    * @param int $zf_order_id
    * @return string
    */
  function admin_notification($zf_order_id) {
    global $db;
    $output = '';
    $trnData->fields = array();
    require(DIR_FS_CATALOG . DIR_WS_MODULES . 'payment/beanstream/beanstream_admin_notification.php');
    return $output;
  }
  /**
   * Used to display error message details
   *
   * @return array
   */
  function get_error() {
    $error = array('title' => MODULE_PAYMENT_BEANSTREAM_TEXT_ERROR,
                   'error' => stripslashes(urldecode($_GET['error'])));
    return $error;
  }
  /**
   * Check to see whether module is installed
   *
   * @return boolean
   */
  function check() {
    global $db;
    if (!isset($this->_check)) {
      $check_query = $db->Execute("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_PAYMENT_BEANSTREAM_STATUS'");
      $this->_check = $check_query->RecordCount();
    }
    return $this->_check;
  }
  /**
   * Install the payment module and its configuration settings
   *
   */
  function install() {
    global $db;
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Beanstream (API) Module', 'MODULE_PAYMENT_BEANSTREAM_STATUS', 'True', 'Do you want to accept Beanstream payments?', '6', '0', 'zen_cfg_select_option(array(\'True\', \'False\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Login Merchant ID (CAD)', 'MODULE_PAYMENT_BEANSTREAM_CAD_LOGIN', '000000000', 'The CAD Merchant ID assigned as your Beanstream account', '6', '0', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Login Merchant ID (USD)', 'MODULE_PAYMENT_BEANSTREAM_USD_LOGIN', '000000000', 'The USD Merchant ID assigned as your Beanstream account', '6', '0', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function) values ('Transaction Username (if any)', 'MODULE_PAYMENT_BEANSTREAM_USERNAME', '', 'You can set a username/password to help secure your transaction data. In your Beanstream Admin Member Area, click on Administration->Account Settings->Order Settings and check the box next to [Use username/password validation against transaction], and supply username/password information.)', '6', '0', now(), 'zen_cfg_password_display')");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function) values ('Transaction Password (if any)', 'MODULE_PAYMENT_BEANSTREAM_PASSWORD', '', 'Enter the password associated with the transaction username specified above.', '6', '0', now(), 'zen_cfg_password_display')");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Authorization Type', 'MODULE_PAYMENT_BEANSTREAM_AUTHORIZATION_TYPE', 'Authorize', 'Do you want submitted credit card transactions to be authorized only, or authorized and captured?', '6', '0', 'zen_cfg_select_option(array(\'Authorize\', \'Authorize+Capture\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Database Storage', 'MODULE_PAYMENT_BEANSTREAM_STORE_DATA', 'False', 'Do you want to save the gateway communications data to the database?', '6', '0', 'zen_cfg_select_option(array(\'True\', \'False\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Require CVD Number?', 'MODULE_PAYMENT_BEANSTREAM_USE_CVV', 'False', 'Do you want to ask the customer for their card\'s CVV/CVD number? Using CVV validation gives better security for transaction validation. If you have the {Require CVD number for credit card transactions} option enabled in your Beanstream Account Settings, you must set this option to True.', '6', '0', 'zen_cfg_select_option(array(\'True\', \'False\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort order of display.', 'MODULE_PAYMENT_BEANSTREAM_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '0', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Payment Zone (restrict to)', 'MODULE_PAYMENT_BEANSTREAM_ZONE', '0', 'If a zone is selected, only enable this payment method for that zone.', '6', '2', 'zen_get_zone_class_title', 'zen_cfg_pull_down_zone_classes(', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, use_function, date_added) values ('Set Completed Order Status', 'MODULE_PAYMENT_BEANSTREAM_ORDER_STATUS_ID', '2', 'Set the status of orders made with this payment module to this value', '6', '0', 'zen_cfg_pull_down_order_statuses(', 'zen_get_order_status_name', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, use_function, date_added) values ('Set Refunded Order Status', 'MODULE_PAYMENT_BEANSTREAM_REFUNDED_ORDER_STATUS_ID', '1', 'Set the status of refunded orders to this value', '6', '0', 'zen_cfg_pull_down_order_statuses(', 'zen_get_order_status_name', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Debug Mode', 'MODULE_PAYMENT_BEANSTREAM_DEBUGGING', 'Off', 'Would you like to enable debug mode?  A complete detailed log of failed transactions may be emailed to the store owner.', '6', '0', 'zen_cfg_select_option(array(\'Off\', \'Alerts Only\', \'Log File\', \'Log and Email\'), ', now())");

		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable VBV/SC', 'MODULE_PAYMENT_BEANSTREAM_ENABLE_VBVSC', 'Off', 'Would you like to enable VBV/SC for Visa/Mastercard?', '6', '0', 'zen_cfg_select_option(array(\'Off\', \'On\'), ', now())");
    // Now do database-setup:
    global $sniffer;
    if (!$sniffer->table_exists(TABLE_BEANSTREAM)) {
      $sql = "CREATE TABLE " . TABLE_BEANSTREAM . " (
                  id int(11) unsigned NOT NULL auto_increment,
                  customer_id varchar(11) NOT NULL default '',
                  order_id int(11) NOT NULL default 0,
                  response_code varchar(8) NOT NULL default '',
                  response_text varchar(250) NOT NULL default '',
                  authorization_type varchar(10) NOT NULL default '',
                  transaction_id varchar(64) NOT NULL default '',
                  sent text NOT NULL default '',
                  received text NOT NULL default '',
                  approval_code varchar(64) NOT NULL default '',
                  time varchar(50) NOT NULL default '',
                  session_id varchar(128) NOT NULL default '',
                  PRIMARY KEY  (id),
                  KEY idx_customer_id_zen (customer_id),
                  KEY idx_order_id_zen (order_id)
                  )";
      $db->Execute($sql);
    }
  }
  /**
   * Remove the module and all its settings
   *
   */
  function remove() {
    global $db;
    $db->Execute("delete from " . TABLE_CONFIGURATION . " where configuration_key like 'MODULE\_PAYMENT\_BEANSTREAM\_%'");
  }
  /**
   * Internal list of configuration keys used for configuration of the module
   *
   * @return array
   */
  function keys() {
    return array('MODULE_PAYMENT_BEANSTREAM_STATUS', 'MODULE_PAYMENT_BEANSTREAM_CAD_LOGIN', 'MODULE_PAYMENT_BEANSTREAM_USD_LOGIN', 'MODULE_PAYMENT_BEANSTREAM_USERNAME', 'MODULE_PAYMENT_BEANSTREAM_PASSWORD', 'MODULE_PAYMENT_BEANSTREAM_AUTHORIZATION_TYPE', 'MODULE_PAYMENT_BEANSTREAM_STORE_DATA', 'MODULE_PAYMENT_BEANSTREAM_USE_CVV', 'MODULE_PAYMENT_BEANSTREAM_SORT_ORDER', 'MODULE_PAYMENT_BEANSTREAM_ZONE', 'MODULE_PAYMENT_BEANSTREAM_ORDER_STATUS_ID', /*'MODULE_PAYMENT_BEANSTREAM_REFUNDED_ORDER_STATUS_ID', */'MODULE_PAYMENT_BEANSTREAM_DEBUGGING', 'MODULE_PAYMENT_BEANSTREAM_ENABLE_VBVSC');
  }
  /**
   * Send communication request
	 * @param submit_data
	 * @param url. Optional url to send data to beanstram e.g. https://www.beanstream.com/scripts/process_transaction_auth.asp
   */
  function _sendRequest($submit_data, $vbv_sc = false) {

		$url = 'https://www.beanstream.com/scripts/process_transaction.asp';
		if( $vbv_sc )
		{
			$url = "https://www.beanstream.com/scripts/process_transaction_auth.asp";
			//$submit_data = array_merge(array(
                         //'requestType' => 'BACKEND', // Force API mode
        //                 ), $submit_data);
		}
		else
		{
			$submit_data = array_merge(array(
                         'merchant_id' => trim($this->login),
                         'username' => trim(MODULE_PAYMENT_BEANSTREAM_USERNAME),
                         'password' => trim(MODULE_PAYMENT_BEANSTREAM_PASSWORD),
                         'requestType' => 'BACKEND', // Force API mode
                         'trnLanguage' => $_SESSION['languages_code'],
                         ), $submit_data);
		}
    // Populate an array that contains all of the data to be sent to the gateway
    

    // set URL
    //$url = 'https://www.beanstream.com/scripts/process_transaction.asp';

    // concatenate the submission data into $data variable after sanitizing
    $data = '';
    while(list($key, $value) = each($submit_data)) {
      $value = str_replace(array('"',"'",'&amp;','&', '='), '', $value);
      $data .= $key . '=' . urlencode($value) . '&';
    }
    // Remove the last "&" from the string
    $data = substr($data, 0, -1);


    // prepare a copy of submitted data for error-reporting purposes
    $this->reportable_submit_data = $submit_data;
		if( !$vbv_sc )
		{
    	$this->reportable_submit_data['merchant_id'] = '*******';
    	$this->reportable_submit_data['username'] = '*******';
    	$this->reportable_submit_data['password'] = '*******';
		}
    if (isset($this->reportable_submit_data['trnCardNumber'])) $this->reportable_submit_data['trnCardNumber'] = str_repeat('X', strlen($this->reportable_submit_data['trnCardNumber'] - 4)) . substr($this->reportable_submit_data['trnCardNumber'], -4);
    if (isset($this->reportable_submit_data['trnCardCvd'])) $this->reportable_submit_data['trnCardCvd'] = '****';
    $this->reportable_submit_data['url'] = $url;
		$this->reportable_submit_data['post_data_sent'] = $data;


    // Post order info data to Beanstream via CURL - Requires that PHP has cURL support installed

    // Send CURL communication
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); /* compatibility for SSL communications on some Windows servers (IIS 5.0+) */
    if (CURL_PROXY_REQUIRED == 'True') {
      $this->proxy_tunnel_flag = (defined('CURL_PROXY_TUNNEL_FLAG') && strtoupper(CURL_PROXY_TUNNEL_FLAG) == 'FALSE') ? false : true;
      curl_setopt ($ch, CURLOPT_HTTPPROXYTUNNEL, $this->proxy_tunnel_flag);
      curl_setopt ($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
      curl_setopt ($ch, CURLOPT_PROXY, CURL_PROXY_SERVER_DETAILS);
    }

    $this->gwresponse = curl_exec($ch);
    $this->commError = curl_error($ch);
    $this->commErrNo = curl_errno($ch);

    $this->commInfo = @curl_getinfo($ch);
    curl_close ($ch);

    // if in 'echo' mode, dump the returned data to the browser and stop execution
    if ((defined('BEANSTREAM_DEVELOPER_MODE') && BEANSTREAM_DEVELOPER_MODE == 'echo') || MODULE_PAYMENT_BEANSTREAM_DEBUGGING == 'echo') {
      echo $this->gwresponse . ($this->commErrNo != 0 ? '<br />' . $this->commErrNo . ' ' . $this->commError : '') . '<br />';
      die('Press the BACK button in your browser to return to the previous page.');
    }

    // parse the data received back from the gateway
    $pairs = explode('&', str_replace(array("\r\n","\n"), '', $this->gwresponse));
    //$this->log('['.$string . "]\n\n[" . print_r($pairs, true) .']');
    $response = array();
    foreach ($pairs as $pair) {
      list($name, $value) = explode('=', $pair);
      $response[$name] = $value;
    }

//if(substr($txResult, 0, 13)=="trnApproved=1") ===== APPROVED

///// ERRORS ////
/*
 * errorType --- N=None, S=System-Error, U=User-Error
 * errorMessage --- descriptive message of all errors in the transaction -- can be used to help customer correct mistakes and resubmit
 * errorFields --- comma-separated list of all fields containing invalid data -- could be used for custom order-handling or to flag fields needing attention
 * NOTE: errorMessage and errorFields are urlencoded.
 */
    return $response;
  }
  /**
   * Used to do any debug logging / tracking / storage as required.
   */
  function _debugActions($response, $order_time= '', $sessID = '', $vbv_sc=false) {
    global $db, $messageStack;
    if ($order_time == '') $order_time = date("F j, Y, g:i a");
    // convert output to 1-based array for easier understanding:
    $resp_output = array_reverse($response);
    $resp_output[] = 'Response from gateway';
    $resp_output = array_reverse($resp_output);

    // DEBUG LOGGING
      $errorMessage = date('M-d-Y h:i:s') .
                      "\n=================================\n\n" .
                      ($this->commError !='' ? 'Comm results: ' . $this->commErrNo . ' ' . $this->commError . "\n\n" : '') .
                      'Response Code: ' . $response['messageId'] . ".\nResponse Text: " . urldecode($response['messageText']) . "\n\n" .
                      'Sending to Beanstream: ' . print_r($this->reportable_submit_data, true) . "\n\n" .
                      'Results Received back from Beanstream: ' . print_r($resp_output, true) . "\n\n" .
                      'CURL communication info: ' . print_r($this->commInfo, true) . "\n";
      if (CURL_PROXY_REQUIRED == 'True')
        $errorMessage .= 'Using CURL Proxy: [' . CURL_PROXY_SERVER_DETAILS . ']  with Proxy Tunnel: ' .($this->proxy_tunnel_flag ? 'On' : 'Off') . "\n";
      $errorMessage .= "\nRAW data received: \n" . $this->gwresponse . "\n\n";

      if (strstr(MODULE_PAYMENT_BEANSTREAM_DEBUGGING, 'Log') || strstr(MODULE_PAYMENT_BEANSTREAM_DEBUGGING, 'All') || true) {
        $key = $response['trnOrderNumber'] . '_' . time() . '_' . zen_create_random_value(4);
        $file = $this->_logDir . '/' . 'Beanstream_Debug_' . $key . '.log';
        if ($fp = @fopen($file, 'a')) {
          fwrite($fp, $errorMessage);
          fclose($fp);
        }
      }
      if (($response['trnApproved'] != '1' && $response['errorType'] != 'N' && stristr(MODULE_PAYMENT_BEANSTREAM_DEBUGGING, 'Alerts')) || strstr(MODULE_PAYMENT_BEANSTREAM_DEBUGGING, 'Email')) {
        zen_mail(STORE_NAME, STORE_OWNER_EMAIL_ADDRESS, 'Beanstream Alert ' . $response['trnOrderNumber'] . ' ' . date('M-d-Y h:i:s') . ' ' . ($response['trnApproved'] == 0 ? 'ERROR=' . $response['messageId'] : ''), $errorMessage, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, array('EMAIL_MESSAGE_HTML'=>nl2br($errorMessage)), 'debug');
      }
      $response_code = $response['messageId'];
      $response_text = urldecode($response['messageText']);
      $response_alert = '(' . $response_code . ') ' . $response_text . ($this->commError == '' ? '' : ' Communications Error - Please notify webmaster.');
      $this->reportable_submit_data['Note'] = $refundNote;

    // DATABASE SECTION
    // Insert the send and receive response data into the database.
    // This can be used for testing or for implementation in other applications
    // This can be turned on and off if the Admin Section
    if (MODULE_PAYMENT_BEANSTREAM_STORE_DATA == 'True'){
      $db_response_text = ($response['messageId'] == 16) ? ' DUPLICATE TRANSACTION ATTEMPT ' : '';
      $db_response_text .= urldecode($response['messageText']) . ($this->commError !='' ? ' - Comm results: ' . $this->commErrNo . ' ' . $this->commError : '');

      // Insert the data into the database
      $sql = "insert into " . TABLE_BEANSTREAM . "  (id, customer_id, order_id, response_code, response_text, authorization_type, transaction_id, sent, received, time, session_id) values (NULL, :custID, :orderID, :respCode, :respText, :authType, :transID, :sentData, :recvData, :orderTime, :sessID )";
      $sql = $db->bindVars($sql, ':custID', $_SESSION['customer_id'], 'integer');
      $sql = $db->bindVars($sql, ':orderID', preg_replace('/[^0-9]/', '', $response['trnOrderNumber']), 'integer');
      $sql = $db->bindVars($sql, ':respCode', $response['messageId'], 'integer');
      $sql = $db->bindVars($sql, ':respText', $db_response_text, 'string');
      $sql = $db->bindVars($sql, ':authType', $response['trnType'], 'string');
      $sql = $db->bindVars($sql, ':transID', ($this->transaction_id != '' ? $this->transaction_id : $response['trnId']), 'string');
      $sql = $db->bindVars($sql, ':sentData', print_r($this->reportable_submit_data, true), 'string');
      $sql = $db->bindVars($sql, ':recvData', print_r($response, true), 'string');
      $sql = $db->bindVars($sql, ':orderTime', $order_time, 'string');
      $sql = $db->bindVars($sql, ':sessID', $sessID, 'string');
      $db->Execute($sql);
    }
  }
  /**
   * Used to submit a refund for a given transaction.
   */
  function _doRefund($oID, $amount = 0) {
    global $db, $messageStack;
    $this->updateMerchantByOID($oID);
    $new_order_status = (int)MODULE_PAYMENT_BEANSTREAM_REFUNDED_ORDER_STATUS_ID;
    if ($new_order_status == 0) $new_order_status = 1;
    $proceedToRefund = true;
    $refundNote = strip_tags(zen_db_input($_POST['refnote']));
    if (isset($_POST['refconfirm']) && $_POST['refconfirm'] != 'on') {
      $messageStack->add_session(MODULE_PAYMENT_BEANSTREAM_TEXT_REFUND_CONFIRM_ERROR, 'error');
      $proceedToRefund = false;
    }
    if (isset($_POST['buttonrefund']) && $_POST['buttonrefund'] == MODULE_PAYMENT_BEANSTREAM_ENTRY_REFUND_BUTTON_TEXT) {
      $refundAmt = (float)$_POST['refamt'];
      $new_order_status = (int)MODULE_PAYMENT_BEANSTREAM_REFUNDED_ORDER_STATUS_ID;
      if ($refundAmt == 0) {
        $messageStack->add_session(MODULE_PAYMENT_BEANSTREAM_TEXT_INVALID_REFUND_AMOUNT, 'error');
        $proceedToRefund = false;
      }
    }
    if (isset($_POST['trans_id']) && trim($_POST['trans_id']) == '') {
      $messageStack->add_session(MODULE_PAYMENT_BEANSTREAM_TEXT_TRANS_ID_REQUIRED_ERROR, 'error');
      $proceedToRefund = false;
    }

    /**
     * Submit refund request to gateway
     */
    if ($proceedToRefund) {
      $submit_data = array('trnType' => 'R',
                           'trnAmount' => number_format($refundAmt, 2),
                           'adjId' => trim($_POST['trans_id'])
                           );
      unset($response);
      $response = $this->_sendRequest($submit_data);
      $response_code = $response['messageId'];
      $response_text = urldecode($response['messageText']);
      $response_alert = '(' . $response_code . ') ' . $response_text . ($this->commError == '' ? '' : ' Communications Error - Please notify webmaster.');
      $this->reportable_submit_data['Note'] = $refundNote;
      $this->_debugActions($response);

      if ($response['trnApproved'] == 0 || $response['errorType'] != 'N') {
        $messageStack->add_session($response_alert, 'error');
      } else {
        // Success, so save the results
        $sql_data_array = array('orders_id' => $oID,
                                'orders_status_id' => (int)$new_order_status,
                                'date_added' => 'now()',
                                'comments' => 'REFUND INITIATED. Trans ID:' . $response['trnId'] . "\n" . ' Refund Amt: ' . number_format($refundAmt, 2) . "\n" . $refundNote,
                                'customer_notified' => 0
                             );
        zen_db_perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data_array);
        $db->Execute("update " . TABLE_ORDERS  . "
                      set orders_status = '" . (int)$new_order_status . "'
                      where orders_id = '" . (int)$oID . "'");
        $messageStack->add_session(sprintf(MODULE_PAYMENT_BEANSTREAM_TEXT_REFUND_INITIATED, $response['trnId'], $response['authCode']), 'success');
        return true;
      }
    }
    return false;
  }

  /**
   * Used to capture part or all of a given previously-authorized transaction.
   */
  function _doCapt($oID, $amt = 0, $currency = 'CAD') {
    global $db, $messageStack;
    $this->updateMerchantByOID($oID);

    //@TODO: Read current order status and determine best status to set this to
    $new_order_status = (int)MODULE_PAYMENT_BEANSTREAM_ORDER_STATUS_ID;
    if ($new_order_status == 0) $new_order_status = 1;

    $proceedToCapture = true;
    $captureNote = strip_tags(zen_db_input($_POST['captnote']));
    if (isset($_POST['captconfirm']) && $_POST['captconfirm'] == 'on') {
    } else {
      $messageStack->add_session(MODULE_PAYMENT_BEANSTREAM_TEXT_CAPTURE_CONFIRM_ERROR, 'error');
      $proceedToCapture = false;
    }
    if (isset($_POST['btndocapture']) && $_POST['btndocapture'] == MODULE_PAYMENT_BEANSTREAM_ENTRY_CAPTURE_BUTTON_TEXT) {
      $captureAmt = (float)$_POST['captamt'];
/*
      if ($captureAmt == 0) {
        $messageStack->add_session(MODULE_PAYMENT_BEANSTREAM_TEXT_INVALID_CAPTURE_AMOUNT, 'error');
        $proceedToCapture = false;
      }
*/
    }
    if (isset($_POST['captauthid']) && trim($_POST['captauthid']) != '') {
      // okay to proceed
    } else {
      $messageStack->add_session(MODULE_PAYMENT_BEANSTREAM_TEXT_TRANS_ID_REQUIRED_ERROR, 'error');
      $proceedToCapture = false;
    }
    /**
     * Submit capture request to the gateway
     */
    if ($proceedToCapture) {
      // Populate an array that contains all of the data to be sent to the gateway
      unset($submit_data);
      $submit_data = array(
                           'trnType' => 'PAC',
                           'trnAmount' => number_format($captureAmt, 2),
                           'adjId' => strip_tags(trim($_POST['captauthid'])),
//                         'trnOrderNumber' => $new_order_id,
//                         'orderShipping' => $order->info['shipping_cost'],
//                         'orderTax' => $order->info['tax'],
                           );

      $response = $this->_sendRequest($submit_data);
      $response_code = $response['messageId'];
      $response_text = urldecode($response['messageText']);
      $response_alert = '(' . $response_code . ') ' . $response_text . ($this->commError == '' ? '' : ' Communications Error - Please notify webmaster.');
      $this->reportable_submit_data['Note'] = $captureNote;
      $this->_debugActions($response);

      if ($response['trnApproved'] == 0 || $response['errorType'] != 'N') {
        $messageStack->add_session($response_alert, 'error');
      } else {
        // Success, so save the results
        $sql_data_array = array('orders_id' => (int)$oID,
                                'orders_status_id' => (int)$new_order_status,
                                'date_added' => 'now()',
                                'comments' => 'FUNDS COLLECTED. Auth Code: ' . $response['authCode'] . "\n" . 'Trans ID: ' . $response['trnId'] . "\n" . ' Amount: ' . $submit_data['trnAmount'] . "\n" . 'Time: ' . date('Y-m-D h:i:s') . "\n" . $captureNote,
                                'customer_notified' => 0
                             );
        zen_db_perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data_array);
        $db->Execute("update " . TABLE_ORDERS  . "
                      set orders_status = '" . (int)$new_order_status . "'
                      where orders_id = '" . (int)$oID . "'");
        $messageStack->add_session(sprintf(MODULE_PAYMENT_BEANSTREAM_TEXT_CAPT_INITIATED, $submit_data['trnAmount'], $response['trnId'], $response['authCode']), 'success');
        return true;
      }
    }
    return false;
  }
  /**
   * Used to void a given previously-authorized transaction.
   */
  function _doVoid($oID, $note = '') {
    global $db, $messageStack;
    $this->updateMerchantByOID($oID);
    $new_order_status = (int)MODULE_PAYMENT_BEANSTREAM_REFUNDED_ORDER_STATUS_ID;
    if ($new_order_status == 0) $new_order_status = 1;
    $voidNote = strip_tags(zen_db_input($_POST['voidnote'] . $note));
    $voidAuthID = trim(strip_tags(zen_db_input($_POST['voidauthid'])));
    $proceedToVoid = true;
    if (isset($_POST['ordervoid']) && $_POST['ordervoid'] == MODULE_PAYMENT_BEANSTREAM_ENTRY_VOID_BUTTON_TEXT) {
      if (isset($_POST['voidconfirm']) && $_POST['voidconfirm'] != 'on') {
        $messageStack->add_session(MODULE_PAYMENT_BEANSTREAM_TEXT_VOID_CONFIRM_ERROR, 'error');
        $proceedToVoid = false;
      }
    }
    if ($voidAuthID == '') {
      $messageStack->add_session(MODULE_PAYMENT_BEANSTREAM_TEXT_TRANS_ID_REQUIRED_ERROR, 'error');
      $proceedToVoid = false;
    }
    if (!isset($_POST['voidamt']) || $_POST['voidamt'] == '') {
      $messageStack->add_session(MODULE_PAYMENT_BEANSTREAM_TEXT_AMOUNT_REQUIRED_ERROR, 'error');
      $proceedToVoid = false;
    }
    $voidAmt = (float)$_POST['voidamt'];
    // Populate an array that contains all of the data to be sent to gateway
    $submit_data = array('trnType' => 'VP',
                         'trnAmount' => number_format($voidAmt, 2),
                         'adjId' => trim($voidAuthID) );
    /**
     * Submit void request to Gateway
     */
    if ($proceedToVoid) {
      $response = $this->_sendRequest($submit_data);
      $response_code = $response['messageId'];
      $response_text = urldecode($response['messageText']);
      $response_alert = '(' . $response_code . ') ' . $response_text . ($this->commError == '' ? '' : ' Communications Error - Please notify webmaster.');
      $this->reportable_submit_data['Note'] = $voidNote;
      $this->_debugActions($response);

      if ($response['trnApproved'] == 0 || $response['errorType'] != 'N' ) {
        $messageStack->add_session($response_alert, 'error');
      } else {
        // Success, so save the results
        $sql_data_array = array('orders_id' => (int)$oID,
                                'orders_status_id' => (int)$new_order_status,
                                'date_added' => 'now()',
                                'comments' => 'VOIDED. Trans ID: ' . $response['trnId'] . "\n" . $voidNote,
                                'customer_notified' => 0
                             );
        zen_db_perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data_array);
        $db->Execute("update " . TABLE_ORDERS  . "
                      set orders_status = '" . (int)$new_order_status . "'
                      where orders_id = '" . (int)$oID . "'");
        $messageStack->add_session(sprintf(MODULE_PAYMENT_BEANSTREAM_TEXT_VOID_INITIATED, $response['trnId'], $response['authCode']), 'success');
        return true;
      }
    }
    return false;
  }

  function updateMerchantByOID($oID) {
    global $db;
    $check = $db->Execute("select currency from " . TABLE_ORDERS . " where orders_id = '" . (int)$oID . "'");

    if ($check->fields['currency'] == 'USD') {
           $this->currency_code = 'USD';
           $this->login = MODULE_PAYMENT_BEANSTREAM_USD_LOGIN;
    }

  }
}
